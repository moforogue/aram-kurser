import React, { Component } from 'react';
import LandingPage from '../landing-page/landing-page';
import './login.css';
import CourseList from '../course/course-list/course-list';

class Login extends Component {

    constructor(props) {
        super(props);
        this.login = this.login.bind(this);

        this.state = {
            loginSuccess: false
        };
    }

    loginSuccess = () => {
        this.setState({
            loginSuccess: true
        });
    };

    login = function () {
        let username = document.getElementById("username").value;
        let password = document.getElementById("password").value;
        if ((username === "username" && password === "password") || (username === "student" && password === "123")) {
            this.loginSuccess();
        }
    }

    render() {
        if (this.state.loginSuccess) {
            return (
                <CourseList />
            );  
        }
        return (
            <div>
                <div className="content" id="content">
                    <h3>Logga in</h3>
                    <br/>
                    <form className="login-form" /*onSubmit={this.login}*/ name="login-form">
                      <div>
                        <input id="username" className="username" type="text" placeholder="Username" name="username" autoFocus />
                      </div>
                      <div>
                        <input id="password" className="password" type="password" placeholder="Password" name="password" />
                      </div>
                      <div>
                        <input onClick={this.login} type="button" value="Logga in" />
                      </div>
                      <div id="errorMessageDiv"></div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Login;