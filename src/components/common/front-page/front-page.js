import React, { Component } from 'react';
import './front-page.css';
import Login from '../login/login';

class FrontPage extends Component {

    constructor(props) {
        super(props);
        this.clicker = this.clicker.bind(this);

        this.state = {
            tab: "why-tab"
        };
    }

    getTab = function(tab) {
        if (tab === "why-tab") {
            return "Lorem ipsum dolor sit amet, at assum luptatum vim, an nonumy delenit labores sed. Animal accusata quo at, per ad tale suas conclusionemque, duo appareat pertinax in. Vix labitur complectitur ea. Ex mei probatus theophrastus. Ad vis erant nonumes.";
        } else if (tab === "price-tab") {
            return "Ei mel quas homero suavitate, te quo nemore iuvaret assueverit. Ne zril quaeque delectus sit, sea at nemore melius, pri et erat ponderum definiebas. Mucius concludaturque sed ne, ex iisque luptatum contentiones has. Copiosae delectus efficiantur cum id. Ei duo nusquam perfecto consulatu, est id senserit instructior.";
        } else if (tab === "about-tab") {
            return "Nam sint patrioque temporibus ex, mei graeco eripuit ea. Sit an odio omnis deseruisse. Eum ut fugit propriae neglegentur, an sit congue doming aliquando. Tale munere laoreet cum ex.";
        }
    }

    clicker(tab) {
        this.setState({
            tab: tab
        });
    }

    render() {
        if (this.state.tab === "login-tab") {
            return ( <Login /> );
        }
        return (
            <div>
                <div id="tabs-div" className="tabs-div">
                    <table id="tabs" className="tabs">
                        <tbody>
                            <tr onClick={(e) => {this.clicker("why-tab")}} id="why-tab" className="tab" ><th>Varför localGhost</th></tr>
                            <tr onClick={(e) => {this.clicker("price-tab")}} id="price-tab" className="tab" ><th>Pris</th></tr>
                            <tr onClick={(e) => {this.clicker("about-tab")}} id="about-tab" className="tab" ><th>Om oss</th></tr>
                            <tr onClick={(e) => {this.clicker("login-tab")}} id="login-tab" className="tab" ><th>Logga in</th></tr>
                        </tbody>
                    </table>
                </div>
                <div id="content" className="content">
                    {this.getTab(this.state.tab)}
                </div>
            </div>
        );
    }
}

export default FrontPage;
