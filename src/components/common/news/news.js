import React, { Component } from 'react';
import './news.css';

class News extends Component {

    newsObj = [];

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() {
    }

    render() {
        return (
            <div>
                <div className="news" id="square">
                    <h3>Nyheter</h3>
                    <br/>
                    -Sumobrottare kommer på besök på torsdag.
                    <br/>
                    -Rektorn ska sluta.
                    <br/>
                    -Fel på ventligation och vatten.
                    <br/>
                </div>
            </div>
        );
    }
}

export default News;