import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getCourses } from '../../../../actions/';
import './course-list.css';
// import CourseResource from '../../rest/courseResource';
import CourseListItem from './course-list-item';

class CourseList extends Component {
    static propTypes = {
        backgroundColor: PropTypes.string
    };
    
    static defaultProps = {
        // backgroundColor: '#fff'
    };

    // courseResource = new CourseResource();
    tabsLength = 0;

    //state = {};//utan constructor

    constructor(props) {
        super(props);
        this.selectCourse = this.selectCourse.bind(this);
        
        //triggers component to rerender when something changes in the state
        this.state = {
            courseList: [],
            course: {}
            // backgroundColor: this.props.backgroundColor //this.props kommer från super(props)
        };
    }

    componentDidMount() {
        const { getCourses } = this.props;
        // this.courseObj = this.courseResource.getCourses();
        // this.tabsLength = this.courseObj.length;
        // this.initBackgroundColors();

        getCourses();
    }
/*
    initBackgroundColors = () => {
        const { backgroundColor } = this.state.backgroundColor;
    };
*/
    renderMenu() {
    }

    selectCourse(course) {
        this.setState({
            course: course
        });
    }

    render() {
        // const { courseList } = this.state;
        const { courses } = this.props;
        // const courses = this.props.courses;
        const state = this.state;
        const selectCourse = this.selectCourse;
        const course = this.state.course;

        return (
            <div>
                <div id="tabs-div" className="tabs-div">
                    <table id="tabs" className="tabs">
                        <tbody>
                            {courses.map(function(course, index){
                                return <CourseListItem onClick={(e) => {selectCourse(course)}} course={course} key={index} />;
                            })}
                        </tbody>
                    </table>
                </div>
                <div id="content" className="content">
                    {console.log(this.state.course)}
                    <h3>{this.state.course.title}</h3>
                    <p>{this.state.course.description}</p>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { courses: state.courses };
};

export default connect(mapStateToProps, { getCourses })(CourseList);
