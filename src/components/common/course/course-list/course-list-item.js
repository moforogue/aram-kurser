import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CourseListItem extends Component {
    static propTypes = {
        course: PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
        })
    };
    
    static defaultProps = {
        course: {
            id: -1,
            title: '',
        }
    };

    

    render() {
        const { id, title } = this.props.course;
        return <tr onClick={this.props.onClick} id="tab" className="tab" key={id}><th>{title}</th></tr>;
    }
}

export default CourseListItem;
