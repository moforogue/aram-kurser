import React, { Component } from 'react';
import './show-form.css';
import Form from '../form/form'

class ShowForm extends Component {

  constructor(props) {
    super(props);
    //när state ändras render sidan
    this.state = {
      showForm: false
    };
  }

  showForm = () => {
    this.setState({
      showForm: true
    });
  };

  render() {
    if (this.state.showForm) {
      return (
        <Form />
      );  
    }
    return (
      <button className="show-form-button" onClick={this.showForm}>+</button>
    );
  }
}

export default ShowForm;
