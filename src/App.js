import React, { Component } from 'react';
import './App.css';
import FrontPage from './components/common/front-page/front-page';
import News from './components/common/news/news';
import CourseList from './components/common/course/course-list/course-list'

class App extends Component {
  render() {
    return (
      <div className="App">
        <CourseList />
        <News />
        <FrontPage />
        
      </div>
    );
  }
}

export default App;


/*
import ShowForm from './components/common/show-form/show-form';
import Login from './components/common/login/login';

<CourseList />
<News />
<Login />

*/