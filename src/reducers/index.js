import { combineReducers } from 'redux';
import courses from './courses';

const rootReducer = combineReducers({
    courses: courses // [] Array
});

export default rootReducer;
