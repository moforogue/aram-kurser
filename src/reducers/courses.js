const initialState = [];

export const courses = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_COURSES':
      return [...state, ...action.payload.courses];
    default:
      return state;
  }
}
export default courses;
