import { GET_COURSES } from './types';

export const getCourses = function getCourses() {
    const xmlHttp = new XMLHttpRequest();
    xmlHttp.open( 'GET', 'http://localhost:8080/course/', false ); // false for synchronous request
    xmlHttp.send( null );
    //JSON object of all news
    const courses = JSON.parse(xmlHttp.responseText);
    
    return {
        type: GET_COURSES,
        payload: {
            courses
        } 
    };
}
